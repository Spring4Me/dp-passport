package net.chenlin.dp.ids.client.util;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

/**
 * ant 地址匹配工具类
 * @author zcl<yczclcn@163.com>
 */
public class AntPathUtil {

    private static final PathMatcher ANT_PATCHER = new AntPathMatcher();

    /**
     * 使用spring antPathMatcher实现
     * @param pattern
     * @param path
     * @return
     */
    public static boolean doMatch(String pattern, String path) {
        return ANT_PATCHER.match(pattern, path);
    }

}
