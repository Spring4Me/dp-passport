package net.chenlin.dp.ids.common.util;

import net.chenlin.dp.ids.common.constant.IdsConst;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * cookie工具类
 * @author zcl<yczclcn@163.com>
 */
public class CookieUtil {

    /**
     * 写cookie，指定domain
     * @param response
     * @param key
     * @param value
     * @param domain
     * @param isRemember
     */
    public static void set(HttpServletResponse response, String key, String value, String domain, boolean isRemember) {
        int maxAge = isRemember ? IdsConst.COOKIE_MAX_AGE : -1;
        set(response, key, value, domain, IdsConst.COOKIE_PATH, maxAge, true);
    }

    /**
     * 写cookie，不指定domain
     * @param response
     * @param key
     * @param value
     * @param isRemember
     */
    public static void set(HttpServletResponse response, String key, String value, boolean isRemember) {
        int maxAge = isRemember ? IdsConst.COOKIE_MAX_AGE : -1;
        set(response, key, value, null, IdsConst.COOKIE_PATH, maxAge, true);
    }

    /**
     * 写cookie
     * @param response
     * @param key
     * @param value
     * @param domain
     * @param path
     * @param maxAge
     * @param isHttpOnly
     */
    public static void set(HttpServletResponse response, String key, String value, String domain, String path,
                           int maxAge, boolean isHttpOnly) {
        Cookie cookie = new Cookie(key, value);
        if (CommonUtil.strIsNotEmpty(domain)) {
            cookie.setDomain(domain);
        }
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        cookie.setHttpOnly(isHttpOnly);
        response.addCookie(cookie);
    }

    /**
     * 获取值
     * @param request
     * @param key
     * @return
     */
    public static String getVal(HttpServletRequest request, String key) {
        Cookie cookie = getCookie(request, key);
        if (cookie != null) {
            return cookie.getValue();
        }
        return null;
    }

    /**
     * 获取cookie
     * @param request
     * @param key
     * @return
     */
    public static Cookie getCookie(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(key)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * 删除cookie，不指定domain
     * @param request
     * @param response
     * @param key
     */
    public static void remove(HttpServletRequest request, HttpServletResponse response, String key) {
        Cookie cookie = getCookie(request, key);
        if (cookie != null) {
            set(response, key, "", null, IdsConst.COOKIE_PATH, 0, true);
        }
    }

    /**
     * 删除cookie，指定domain
     * @param request
     * @param response
     * @param key
     */
    public static void remove(HttpServletRequest request, HttpServletResponse response, String key, String domain) {
        Cookie cookie = getCookie(request, key);
        if (cookie != null) {
            set(response, key, "", domain, IdsConst.COOKIE_PATH, 0, true);
        }
    }

}
