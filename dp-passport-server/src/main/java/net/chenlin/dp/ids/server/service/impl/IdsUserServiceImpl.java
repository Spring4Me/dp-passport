package net.chenlin.dp.ids.server.service.impl;

import net.chenlin.dp.ids.server.dao.IdsUserMapper;
import net.chenlin.dp.ids.server.entity.IdsUserEntity;
import net.chenlin.dp.ids.server.service.IdsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 用户service
 * @author zcl<yczclcn@163.com>
 */
@Service("idsUserService")
public class IdsUserServiceImpl implements IdsUserService {

    @Autowired
    private IdsUserMapper idsUserMapper;

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    @Override
    public IdsUserEntity getByUserName(String username) {
        return idsUserMapper.getByUserName(username);
    }

    /**
     * 更新用户最近登录时间
     * @param userId
     * @return
     */
    @Override
    public Integer updateUserLastLoginTime(Long userId) {
        IdsUserEntity userEntity = new IdsUserEntity();
        userEntity.setId(userId);
        userEntity.setGmtLastLogin(new Date());
        return idsUserMapper.updateUserLastLoginTime(userEntity);
    }

}
